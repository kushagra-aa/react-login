import React ,{useState} from 'react';
import LoginForm from './LoginForm';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure()

function ForgotPassword()
{
    const[details,setDetails]=useState({email:"",dob:""});
    const submitHandler= e=>
    {
        e.preventDefault();
        Forgot(details);
    }

    const adminUser={
        name:"root",
        email:"root@admin.com",
        password:"root1234",
        dob:"01/01/2000"
      }
      const[user,setUser]=useState({name:"",email:""});
      const[error,setError]=useState("");
        const Forgot=details=> {
        console.log(details);

      const notify=()=>{
          toast.success("password: root1234",{autoClose:5000});
      }  
    
        if(details.email==adminUser.email && details.dob==adminUser.dob)
        {
          console.log("Logged in");
          setUser({
            email:details.email,
            dob:details.dob
          });
          notify();
        }
        else
        {
          console.log("Details do not match");
          setError("Details do not match");
        }
      }

    return(
        <form onSubmit={submitHandler}>
            <div className="form-inner">
                <h2>Forgot Password</h2>
                {(error!="")?(<div className="error">{error}</div>): ""}
                <div className="form-group">
                    <label htmlFor="email">Email:</label>
                    <input type="email" name="email" id="email" onChange={e=> setDetails({...details, email:e.target.value})} value={details.email}/>
                </div>
                <div className="form-group">
                    <label htmlFor="dob">DOB:</label>
                    <input type="dob" name="dob" id="dob" onChange={e=> setDetails({...details, dob:e.target.value})} value={details.dob}/>
                </div>

                <input type="submit" value="PASSWORD"/>
                </div>
        </form>

    )
}


export default ForgotPassword;