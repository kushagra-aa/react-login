import React,{useState} from 'react';
import LoginForm from './components/LoginForm';
import ForgotPassword from './components/ForgotPassword';
import {BrowserRouter,Route,Switch,Link,Redirect } from 'react-router-dom';
import ReactNotification from 'react-notification-component';
import {store} from 'react-notification-component';


function App() {
  const adminUser={
    name:"root",
    email:"root@admin.com",
    password:"root1234",
    dob:"01/01/2000"
  }
  const[user,setUser]=useState({name:"",email:""});
  const[error,setError]=useState("");

  const Login=details=> {
    console.log(details);

    if(details.email==adminUser.email && details.password==adminUser.password)
    {
      console.log("Logged in");
      setUser({
        name:details.name,
        email:details.email
      });
    }
    else
    {
      console.log("Details do not match");
      setError("Details do not match");
    }
  }

  const Logout=() => {
    setUser({name:"",email:""});
  }
  
  return (
    <div className="App">
      {(user.email!="") ? (
        <div className="welcome">
          <h2>Welcome, <span> {user.name}</span></h2>
          <button onClick={Logout}>Logout</button>
          </div>
      ):(
          <LoginForm Login={Login} error={error}/>
      )}
      <Switch>
       <Route exact path="/ForgotPassword" component={ForgotPassword}/>
      <Route exact path="/LoginForm" component={LoginForm}/>
      </Switch>
      </div>
    );
}

export default App;




